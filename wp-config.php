<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vydunas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mg6<3lKn^(3W_FCt_ )`g;8&j!L9e-D5gV-)`$=f2C$zY4N/bFXyj!FV!W^;icPp');
define('SECURE_AUTH_KEY',  '?#ncCPcfewf/i0gwF:DvAP]MBOc)[cN<Y)+ZL71q2RZE2iC&T ;q|LU Bx7v,Sc>');
define('LOGGED_IN_KEY',    '`9]@x67YvE>A>Yv+[;P/e>7S ysJf,RAsa0Ga@DCdq=QSw1uNgc25eCGTBiZ*>{>');
define('NONCE_KEY',        'x+gsGZ|G8B9.OA:.S:Hj;vd&g<myzOX%Orxc.c8$Y+Rx<sb)q-c5[HPhE8oH_X7%');
define('AUTH_SALT',        'Fn7 (8FRrAQ_UIn [uF?D7Bd>MI@w9J.eT:sQ16VkN@pR;/mX2s#_eUjdr,_sOJ!');
define('SECURE_AUTH_SALT', 'rz*qE7u9KE{$FJ$L%Rnt+3a.jslnzH%PUody97LDdp)dO/qY1Aw{p^fq{h56=q*:');
define('LOGGED_IN_SALT',   'xfTo/s)mCE8D0D_rBP8|@~.3cyb$7s%VBvXLhJf!oI12mv&.A?Xa4[(J;<H.grlZ');
define('NONCE_SALT',       '#Dcz{?c:?/pMd<C+<3>+HB]M(L^+)*^]q>sLGlTN$nXYyyuGx(75zT5`X&dyAC$b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
