<?php get_header(); ?>

<?php get_template_part("template-parts/index-slider"); ?>

<?php get_sidebar("left"); ?>

<?php if(!is_active_sidebar("sidebar-right") && !is_active_sidebar("sidebar-left")) : ?>
	<div class="col-xs-12 col-md-12">
<?php elseif(!is_active_sidebar("sidebar-right") || !is_active_sidebar("sidebar-left")) : ?>
	<div class="col-xs-12 col-md-9">
<?php else : ?>
	<div class="col-xs-12 col-md-6">
<?php endif; ?>
		<div id="primary">
			<?php
				if(have_posts()) :
					while(have_posts()) : the_post();
						get_template_part("template-parts/post/content", get_post_format());
					endwhile;
				endif;
			?>
		</div>
	</div>

<?php 
	get_sidebar("right");
	get_footer();
?>