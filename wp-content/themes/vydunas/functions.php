<?php


/* 
	Registering widgets
*/

add_action("init", "vyduno_navigation_init");
function vyduno_navigation_init() 
{
	register_nav_menu("main-nav", __("Navigacija prie logotipo"));
	register_nav_menu("slider-nav", __("Navigacija prie nuotraukų"));
}

add_action("init", "vyduno_footer_links_list");
function vyduno_footer_links_list()
{
	register_nav_menu("links-1", __("Nuorodu sarasas #1"));
	register_nav_menu("links-2", __("Nuorodu sarasas #2"));
	register_nav_menu("links-3", __("Nuorodu sarasas #3"));
	register_nav_menu("links-3", __("Nuorodu sarasas #4"));
}

add_action("widgets_init", "vyduno_widgets_init");
function vyduno_widgets_init() 
{
	register_sidebar(array(
		"name" 			=> __('Kairioji panelė', 'vyduno'),
		"id" 			=> 'sidebar-left',
		"description" 	=> 'Kairysis sidebar',
		"before_widget" => '<div id="%1$s" class="lsw widget %2$s">',
		"after_widget"	=> '</div>',
		"before_title" 	=> '<h2>',
		"after_title" 	=> '</h2>',
	));

	register_sidebar(array(
		"name" 			=> __('Deťionioji panelė', 'vyduno'),
		"id" 			=> 'sidebar-right',
		"description"	=> 'Deťinysis sidebar',
		"before_widget" => '<div id="%1$s" class="rsw widget %2$s">',
		"after_widget"	=> '</div>',
		"before_title" 	=> '<h2>',
		"after_title" 	=> '</h2>',
	));

	register_sidebar(array(
		"name"			=> __('Slider galerija', 'vyduno'),
		"id" 			=> 'slider-gallery'
	));
}

function vyduno_scripts() 
{
	wp_enqueue_script('vyduno-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true);
	wp_localize_script("vyduno-skip-link-focus-fix", "vydunoScreenReaderText", $vyduno_l10n);

	if(has_nav_menu("main-nav"))
	{
		wp_enqueue_script("vyduno-navigation", get_theme_file_uri("/assets/js/navigation.js"), array("jquery"), "1.0", true);
	}

}
add_action("wp_enqueue_scripts", "vyduno_scripts");

require get_parent_theme_file_path("/inc/custom-settings.php");
require get_parent_theme_file_path("/inc/template-tags.php");
require get_parent_theme_file_path("/inc/walkers.php");

?>