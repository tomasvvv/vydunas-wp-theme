<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header class="entry-header">
		<?php 
			if(is_single()) 
			{
				the_title("<h1 class='entry-title'>", "</h1>");
			}
			else if(is_front_page() && is_home())
			{
				the_title("<h3 class='entry-title'><a href='".esc_url(get_permalink())."' rel='bookmark'>",
				"</a></h3>");
			}
			else
			{
				the_title("<h2 class='entry-title'><a href='".esc_url(get_permalink())."' rel='bookmark'>",
				"</a></h2>");
			}	
			if(get_post_type() == "post")
			{
				echo "<div class = 'entry-meta'>";
				if(!is_single())
				{
					vyduno_posted_on(); // get time and author
					vyduno_edit_link(); // ad edit link for logged in admin
				}
				echo "</div>";
			}
		?>
	</header>

	<?php if(get_the_post_thumbnail() != "" && !is_single()) : ?>
		<div class="post-thumbnail">
			<a href = "<?php the_permalink(); ?>">
				<?php the_post_thumbnail("vyduno-featured-image"); ?>
			</a>
		</div>
	<?php endif; ?>

	<div class="entry-content">
	 	<?php 
	 		the_content(sprintf(
	 			__("Skaityti daugiau", "vyduno")
	 		));

	 		wp_link_pages(array(
	 			"before" 		=>	"<div class='page-links'>".__("Puslapiai:", "vyduno"),
	 			"after" 		=>	"</div>",
	 			"link_before" 	=>	"<span class='page-number'>",
	 			"link_after"	=>	"</span>",
	 		));
	 	?>
	</div> <!-- .entry-content -->

	<?php 
		if(is_single()) vyduno_entry_footer();
	?>

</article>