<?php 
	function vyduno_posted_on() 
	{
		echo '<span class = "posted-on">'.vyduno_time_link().'</span>';	
	} 

	function vyduno_entry_footer()
	{
		
	}

	function vyduno_time_link()
	{
		$time_string = '<time class = "entry-date published updated" datetime="%1$s">%2$s</time>';
		if(get_the_time("U") !== get_the_modified_time("U")) 
		{
			$time_string = '<time class = "entry-date published" datettime = "%1$s">%2$s</time><time class = "updated" datetime="%3$s">%4$s</time>';
		}
		
		$time_string = sprintf($time_string, 
				get_the_date(DATE_W3C),
				get_the_date(),

				get_the_modified_date(DATE_W3C),
				get_the_modified_date());

		return sprintf(
			__('<span class="screen-reader-text">Publikuota</span> %s', 'vyduno'),
			'<a href = "'.esc_url(get_permalink()).'" rel="bookmark">'.$time_string.'</a>');
	}

	function vyduno_edit_link() {
		edit_post_link(
			sprintf(
				__('Redaguoti <span class="screen-reader-text">"%s"</span>', 'vyduno'),
				get_the_title()
			),
			'<span class = "edit-link">',
			'</span>'
		);
	}

?>