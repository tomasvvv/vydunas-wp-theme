<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo("charset"); ?>">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<meta charset="viewport" content ="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<div id="page" class = "site">
	<div class = "container-fluid">
		<div class = "site-inner">
			<header id = "masthead" class = "page-header">
				<div class="logo-header">
					<div class="site-width row">
						<div class="col-md-4">
							<div class="header-image">
								<a href="<?php echo esc_url(home_url("/")); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" width = "200px" height = "auto" class = "header-logo"/></a>
							</div>
						</div>
						<div class="col-md-8">
							<!--<nav id="site-nav" class="main-nav">
								<div id = "main-menu" class="main-menu-container">
								<?php wp_nav_menu(
												array(
													"theme_location" 	=> "main-nav",
													"menu_id" 			=> "primary-menu"
												)); ?>
								</div>
							</nav>-->
						</div>
					</div>
				</div>
				<div class="site-width quick-links-nav-wrapper">
					<div class="row">
						<div class = "col-md-8">
							<nav class="quick-links-nav">
								<div class="quick-links-nav-list">
									<?php wp_nav_menu(
												array(
													"theme_location" 	=> "quick-links-nav",
													"menu_id" 			=> "quick-menu"
												)); ?>
								</div>
								<div class = "quick-links-nav-search">
									<?php get_search_form(); ?>
								</div>
							</nav>
						</div>
					</div>				
				</div>
			</header>
			<div class = "site-content">
				<div class = "site-width row">

