

	</div> <!-- .site-inner -->
	
	<footer class="page-footer">
		<div class="footer-inner">
			<div class = "site-width row">
				<div class="col-xs-12 col-md-3 padding-left-15-m">
					<h3 class = "footer-title">
						Nuorodos
					</h3>
					<div class="footer-meta">
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
					</div>
				</div>

				<div class="col-xs-12 col-md-3 padding-left-15-m">
					<h3 class = "footer-title">
						Nuorodos
					</h3>
					<div class="footer-meta">
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
					</div>
				</div>

				<div class="col-xs-12 col-md-3 padding-left-15-m">
					<h3 class = "footer-title">
						Nuorodos
					</h3>
					<div class="footer-meta">
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
					</div>
				</div>

				<div class="col-xs-12 col-md-3 padding-left-15-m">
					<h3 class = "footer-title">
						Nuorodos
					</h3>
					<div class="footer-meta">
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
						Nuoroda<br>
					</div>
				</div>

			</div>

			<div class="site-width">
				<hr>
			</div>

			<div class="site-width row">
				<div class="col-md-4 align-left">
				</div>
				<div class="col-md-4 align-center">
					<a href = "#">Klaipėdos Vydūno Gimnazija</a>
				</div>
				<div class="col-md-4 align-right">
					Programavimas: <a href ="http://www.tovo.lt">Tomas Vosylius</a>
				</div>

			</div>
		</div>
	</footer>

	</div> <!-- .container-fluid -->

</div> <!-- .site -->

<?php wp_footer(); ?>

<div id="responsive-menu" class="panel mobile-menu"><?php wp_nav_menu( array('theme_location' => 'main-nav',) );?></div>
<script> jQuery('.menu-link').bigSlide({
       menu: '.mobile-menu',
       speed: 300,
       side:"right",
      easyClose:true});
  </script>
</body>
</html>